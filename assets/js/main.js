$(document).ready(function () {  
    loadAirport();
    initDatePicker();

    // At load time getting the sessionid
   if(!$.cookie("sid")) {

    const data = JSON.stringify({
        "AuthType": "WBS",
        "IsMobile": "N",
        "Password": "Test@1234567ert",
        "Role"    : "A",
        "UserId"  : "81887"
      });
      
      const xhr = new XMLHttpRequest();
      xhr.withCredentials = true;
      
      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
          const res  = JSON.parse(this.responseText)

          $.cookie("JSESSIONID=", res.Data.SessionId, { expires : 5 });

        }
      });
      
      xhr.open("POST", "https://test.sastiticket.com/restwebsvc/auth/loginV1");
      xhr.setRequestHeader("content-type", "application/json");
      
      xhr.send(data);

    }


      //////////////////////////////////////////////////////////////////////////

    const multi = `<div class="radio-group-flt">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" name="flight-radio" id="flight-one"
        value="one" class="custom-control-input" />
        <label for="flight-one" class="custom-control-label">One Way</label>
    </div>
    
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" name="flight-radio" id="flight-round"
        value="round" class="custom-control-input" />
        <label for="flight-round" class="custom-control-label">Round Trip</label>
    </div>
    
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" name="flight-radio" id="flight-multi"
        value="multi" class="custom-control-input" checked/>
        <label for="flight-multi" class="custom-control-label">Multi-city</label>
    </div>
    </div>
    
    
    <div class="mt-4">
    
    <div class="row">
    
        <div class="col-6">
            <div class="form-group">
                <label for="ap-from" class="w-100 text-left">From</label>
                <select class="form-control airport-select2" name="" id="">
                </select>
            </div>
        </div>
    
        <div class="col-6">
            <div class="form-group">
                <label for="ap-to" class="w-100 text-left">To</label>
                <select class="form-control airport-select2" name="" id="">
                </select>
            </div>
        </div>
    
    </div>
    
    <!-- From and to select box end-->
    
    
    <div class="row mt-3">
    
        <div class="col-6">
            <div class="form-group">
                <label for="dep-date" class="w-100 text-left">Departure</label>
                <input data-toggle="datepicker" class="form-control" name="" id="" placeholder="">
            </div>
        </div>
    
        <div class="col-6">
        </div>
    
    </div>

    <div class="row">

        <div class="col-6">
            <div class="form-group">
                <label for="ap-from" class="w-100 text-left">From</label>
                <select class="form-control airport-select2" name="" id="">
                </select>
            </div>
        </div>

        <div class="col-6">
            <div class="form-group">
                <label for="ap-to" class="w-100 text-left">To</label>
                <select class="form-control airport-select2" name="" id="">
                </select>
            </div>
        </div>

    </div>

    <div class="row mt-3">
    
        <div class="col-6">
            <div class="form-group">
                <label for="dep-date" class="w-100 text-left">Departure</label>
                <input data-toggle="datepicker" class="form-control" name="" id="" placeholder="">
            </div>
        </div>

        <div class="col-6" id="default-add-city" style="padding: 2em;">
            <div class="form-group">
                <button type="button" class="btn btn-primary" id="flight-add-city">Add City</button>
            </div>
        </div>

   </div>
    

   <div id="flight-multi-add-city"></div>
    
    <!-- Departure and Return end -->
    
    
    <div class="row mt-3">
    
        <div class="col-3">
            <div class="form-group">
                <label for="cabin-class" class="w-100 text-left l-font-size">Cabin Class</label>
                <select class="form-control" name="" id="cabin-class">
                    <option value="G">Economy</option>
                    <option value="R">Refundable Economy</option>
                    <option value="E">Full Fare Economy</option>
                    <option value="W">Premium Economy</option>
                    <option value="B">Business</option>
                    <option value="F">First</option>
                </select>
            </div>
        </div>
    
        <div class="col-3">
            <div class="form-group">
                <label for="passn-adult" class="w-100 text-left l-font-size">Adults(12+)</label>
                <input type="number" class="form-control" name="" id="passn-adult" placeholder=""
                    value="1">
            </div>
        </div>
    
        <div class="col-3">
            <div class="form-group">
                <label for="" class="w-100 text-left l-font-size">Children(2-11)</label>
                <input type="number" class="form-control" name="" id="passn-children" placeholder=""
                    value="0">
            </div>
        </div>
    
        <div class="col-3">
            <div class="form-group">
                <label for="passn-infant" class="w-100 text-left l-font-size">Infants(0-2)</label>
                <input type="number" class="form-control" name="" id="passn-infant" placeholder=""
                    value="0">
            </div>
        </div>
    
    </div>
    
    </div>
    
    <div class="text-right mt-3">
    <button type="button" name="" id="submit-search" class="btn purple-gradient text-white btn-rounded">SEARCH FLIGHTS</button>
    </div>`;





// one way 

const one_n_round = `<div class="radio-group-flt">
<div class="custom-control custom-radio custom-control-inline">
    <input type="radio" name="flight-radio" id="flight-one"
    value="one" class="custom-control-input" />
    <label for="flight-one" class="custom-control-label">One Way</label>
</div>

<div class="custom-control custom-radio custom-control-inline">
    <input type="radio" name="flight-radio" id="flight-round"
    value="round" class="custom-control-input" />
    <label for="flight-round" class="custom-control-label">Round Trip</label>
</div>

<div class="custom-control custom-radio custom-control-inline">
    <input type="radio" name="flight-radio" id="flight-multi"
    value="multi" class="custom-control-input" />
    <label for="flight-multi" class="custom-control-label">Multi-city</label>
</div>
</div>


<div class="mt-4">

<div class="row">

    <div class="col-6">
        <div class="form-group">
            <label for="ap-from" class="w-100 text-left">From</label>
            <select class="form-control airport-select2" name="" id="ap-from">
            </select>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group">
            <label for="ap-to" class="w-100 text-left">To</label>
            <select class="form-control airport-select2" name="" id="ap-to">
            </select>
        </div>
    </div>

</div>

<!-- From and to select box end-->


<div class="row mt-3">

    <div class="col-6">
        <div class="form-group">
            <label for="dep-date" class="w-100 text-left">Departure</label>
            <input data-toggle="datepicker" class="form-control" name="" id="dep-date" placeholder="">
        </div>
    </div>

    <div class="col-6">
        <div class="form-group">
            <label for="return-date" class="w-100 text-left">Return</label>
            <input data-toggle="datepicker" class="form-control" name="" id="return-date" placeholder="">
        </div>
    </div>

</div>


<!-- Departure and Return end -->


<div class="row mt-3">

    <div class="col-3">
        <div class="form-group">
            <label for="cabin-class" class="w-100 text-left l-font-size">Cabin Class</label>
            <select class="form-control" name="" id="cabin-class">
                <option value="G">Economy</option>
                <option value="R">Refundable Economy</option>
                <option value="E">Full Fare Economy</option>
                <option value="W">Premium Economy</option>
                <option value="B">Business</option>
                <option value="F">First</option>
            </select>
        </div>
    </div>

    <div class="col-3">
        <div class="form-group">
            <label for="passn-adult" class="w-100 text-left l-font-size">Adults(12+)</label>
            <input type="number" class="form-control" name="" id="passn-adult" placeholder=""
                value="1">
        </div>
    </div>

    <div class="col-3">
        <div class="form-group">
            <label for="" class="w-100 text-left l-font-size">Children(2-11)</label>
            <input type="number" class="form-control" name="" id="passn-children" placeholder=""
                value="0">
        </div>
    </div>

    <div class="col-3">
        <div class="form-group">
            <label for="passn-infant" class="w-100 text-left l-font-size">Infants(0-2)</label>
            <input type="number" class="form-control" name="" id="passn-infant" placeholder=""
                value="0">
        </div>
    </div>

</div>

</div>

<div class="text-right mt-3">
<button type="button" name="" id="submit-search" class="btn purple-gradient text-white btn-rounded">SEARCH FLIGHTS</button>
</div>`;



$(document).on('change', 'input[name=flight-radio]', function() {

        if (this.id === 'flight-round') {
            $('#flight-div').html(one_n_round)
            $('#flight-round').prop('checked', true)
        }
        else if (this.id === 'flight-multi') {
            $('#flight-div').html(multi)
        }
        else {
            $('#flight-div').html(one_n_round);
            $('#flight-one').prop('checked', true)
        }

        loadAirport()
        initDatePicker()
    });


    let btn_count = 1;

    function addcity(count) {
        return `<div id="addcity${count}">
                        <div class="row">

                        <div class="col-6">
                            <div class="form-group">
                                <label for="ap-from" class="w-100 text-left">From</label>
                                <select class="form-control airport-select2" name="" id="">
                                </select>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="ap-to" class="w-100 text-left">To</label>
                                <select class="form-control airport-select2" name="" id="">
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="row mt-3">
                    
                        <div class="col-6">
                            <div class="form-group">
                                <label for="dep-date" class="w-100 text-left">Departure</label>
                                <input data-toggle="datepicker" class="form-control" name="" id="" placeholder="">
                            </div>
                        </div>
                        ${count == 2 ? '' : 
                        `<div class="col-3" id="flight-add-${count}" style="padding: 2em;">
                            <div class="form-group">
                                <button type="button" class="btn btn-primary" id="flight-add-city">Add City</button>
                            </div>
                        </div>`}

                        <div class="col-3" id="flight-remove-${count}" style="padding: 2em;">
                            <div class="form-group">
                                <button type="button" class="btn btn-danger" id="flight-remove-city">Remove</button>
                            </div>
                        </div>

                </div>
        
        </div>`;
    }

    // end of addcity


    // Add City Button click event (multi city)
    $('#flight-div').on('click', '#flight-add-city', function () {
        
        if(btn_count < 3) {
            $('#flight-add-city').remove();
            $('#flight-remove-city').remove();
            $('#flight-multi-add-city').before(addcity(btn_count));
            loadAirport()
            initDatePicker()
            btn_count++
        }
    });

    // Remove City Button click event (multi city)
    $('#flight-div').on('click', '#flight-remove-city', function () {
        
            if(btn_count > 1) {
                $(`#addcity${btn_count-1}`).remove();
                if(btn_count == 2) {
                    $('#default-add-city').html('<button type="button" class="btn btn-primary" id="flight-add-city">Add City</button>')
                }
                else {
                    $(`#flight-add-${btn_count-2}`).html('<button type="button" class="btn btn-primary" id="flight-add-city">Add City</button>')
                    $(`#flight-remove-${btn_count-2}`).html('<button type="button" class="btn btn-danger" id="flight-remove-city">Remove</button>')
                }
                loadAirport()
                initDatePicker()
                btn_count--
            }
    });





    // in one way => click on return input then checked the round way
    $('#flight-div').on('click', '#return-date',  function() {
        $('#flight-round').prop('checked', true)
    });



    /**
     * Ajax 
     */

     $(document).on('click','#submit-search', function() {
      
        const  ap_from     = $('#ap-from').val()
        const  ap_to       = $('#ap-to').val()
        const  dep_date    = $('#dep-date').val()
        const  ret_date    = $('#return-date').val()
        const  cabin_class = $('#cabin-class').val()
        const  adult       = $('#passn-adult').val()
        const  children    = $('#passn-children').val()
        const  infant      = $('#passn-infant').val()  

        switch ($("input[name=flight-radio]:checked").val()) {
            case 'one':
                console.log('one')
                searchFlts(ap_from, ap_to, dep_date, '', adult, children, infant, cabin_class)
                break;

            case 'round':
                console.log('round')
                searchFlts(ap_from, ap_to, dep_date, ret_date, adult, children, infant, cabin_class)
                break;

            case 'multi':
                console.log('multi')
                break;
        
            default:
                break;
        }

     });


     /****************************** Search.html Start (@Script Section) *************************/
     


    // @price Filter start

                    const rangeSlider = document.getElementById("rs-range-line");
                    const rangeBullet = document.getElementById("rs-bullet");

                    if(rangeSlider) rangeSlider.addEventListener("input", showSliderValue, false);

                    function showSliderValue() {
                        rangeBullet.innerHTML = rangeSlider.value + ' Rs';
                        // var bulletPosition = (rangeSlider.value /rangeSlider.max);
                        // console.log(bulletPosition)
                        // rangeBullet.style.left = (bulletPosition * 92) + "%";
                        $('#search-main').empty();
                        searchResArrange({price: rangeSlider.value});
                    }

     // @price Filter End






    // @book_flight (radio checking) start


                 $(document).on('change', 'input[type=radio][name=flt_book]', function() { 
                     sessionStorage.setItem('i', $(this).val())
                     $(this).prop('checked', true)
                     
                     const i = $(this).val().split('_') 
                     const i1 = sessionStorage.getItem('i1')

                     const fly_res = JSON.parse(sessionStorage.getItem('f_res'))
                     const fly_res1 = JSON.parse(sessionStorage.getItem('f_res1'))

                    $('#foot_selected_flt').empty();

                    $.each(fly_res.Data.AirSearchResult[i].OptionSegmentsInfo, function (i, v) {

                        const selected_flt = `<div class="row">
                                                    <div class="col-3">
                                                        <img src="assets/img/SG.gif" alt="SpiceJet"> &nbsp;
                                                        <span class="flt_detail">
                                                            <span id="foot_jetname">${v.MarketingAirline}</span> <br>
                                                            <span id="foot_jetmodel">${v.CarrierCode}-${v.FlightNumber}</span>
                                                        </span>
                                                    </div>
        
                                                    <div class="col-2">
                                                        <b id="foot_dep_ap_ti">${v.DepartureTime.split(',')[1].split(':')[0]}:${v.DepartureTime.split(',')[1].split(':')[1]} ${v.DepartureAirport}</b>
                                                    </div>
        
                                                    <div class="col-1">
                                                        <b><i class="fas fa-long-arrow-alt-right"></i></b>
                                                    </div>
        
                                                    <div class="col-2">
                                                        <b id="foot_arr_ap_ti">${v.ArrivalTime.split(',')[1].split(':')[0]}:${v.ArrivalTime.split(',')[1].split(':')[1]} ${v.ArrivalAirport}</b>
                                                    </div>
                                             </div>`;
        
        
        
                        $('#foot_selected_flt').append(selected_flt);
                    });
        
                    $('#foot_totalp').html('INR ' + (Number(fly_res.Data.AirSearchResult[i].OptionPriceInfo.TotalPrice) + Number(fly_res1.Data.AirSearchResult[i1].OptionPriceInfo.TotalPrice)) )

                 });




                 $(document).on('change', 'input[type=radio][name=flt_book1]', function() { 
                    sessionStorage.setItem('i1', $(this).val())
                    $(this).prop('checked', true)
                    
                    const i = $(this).val().split('_') 
                    const i1 = sessionStorage.getItem('i')

                    const fly_res1 = JSON.parse(sessionStorage.getItem('f_res1'))
                    const fly_res = JSON.parse(sessionStorage.getItem('f_res'))

                   $('#foot_selected_flt1').empty();

                   $.each(fly_res1.Data.AirSearchResult[i].OptionSegmentsInfo, function (i, v) {

                       const selected_flt = `<div class="row">
                                                   <div class="col-3">
                                                       <img src="assets/img/SG.gif" alt="SpiceJet"> &nbsp;
                                                       <span class="flt_detail">
                                                           <span id="foot_jetname">${v.MarketingAirline}</span> <br>
                                                           <span id="foot_jetmodel">${v.CarrierCode}-${v.FlightNumber}</span>
                                                       </span>
                                                   </div>
       
                                                   <div class="col-2">
                                                       <b id="foot_dep_ap_ti">${v.DepartureTime.split(',')[1].split(':')[0]}:${v.DepartureTime.split(',')[1].split(':')[1]} ${v.DepartureAirport}</b>
                                                   </div>
       
                                                   <div class="col-1">
                                                       <b><i class="fas fa-long-arrow-alt-right"></i></b>
                                                   </div>
       
                                                   <div class="col-2">
                                                       <b id="foot_arr_ap_ti">${v.ArrivalTime.split(',')[1].split(':')[0]}:${v.ArrivalTime.split(',')[1].split(':')[1]} ${v.ArrivalAirport}</b>
                                                   </div>
                                            </div>`;
       
       
       
                       $('#foot_selected_flt1').append(selected_flt);
                   });
       
       
       
       
                   $('#foot_totalp').html('INR ' + (Number(fly_res1.Data.AirSearchResult[i].OptionPriceInfo.TotalPrice) + Number(fly_res.Data.AirSearchResult[i1].OptionPriceInfo.TotalPrice) ))

                });


    // @book_flight (radio checking) end


                $(document).on('click', '#foot_flt_book_btn', function() {
                    window.location.href = 'flight_bookingdetails.html';
                });


    //*********************************************  search.html End 


            
    //*****************************************************  flight_bookingdetails.html   Start    */

                $('#box_1_proceed-btn').click(function() {
                    $('#box_1').hide()
                    $('#box_2').show()
                    $('#box_3').show()
                });


                $('#box_3_back-btn').click(function() {
                    $('#box_2').hide()
                    $('#box_3').hide()
                    $('#box_1').show()
                });



                $('#box_3_book_flt_btn').click(function() {
                    const title = $('#name_title').val()
                    const fname = $('#f_name').val()
                    const lname = $('#l_name').val()
                    const mob   = $('#mob').val()
                    const email = $('#email').val()

                    const searchFormData = JSON.parse(sessionStorage.getItem('f_res')).Data.SearchFormData;

                    const data = JSON.stringify({
                        "Data": {
                          "AirOriginDestinationOptions": [
                            1
                          ],
                          "SearchFormData": searchFormData,
                        }
                      });
                      
                      const xhr = new XMLHttpRequest();
                      xhr.withCredentials = true;
                      
                      xhr.addEventListener("readystatechange", function () {
                        if (this.readyState === 4) {

                            const f_reprice      = JSON.parse(this.responseText)

                            const CartData       = f_reprice.Data.CartData
                            const SearchFormData = f_reprice.Data.SearchFormData
                            const CartBookingId  = f_reprice.Data.Cart.CartBookingId

                            const data = JSON.stringify({
                                "Data": {
                                  "SearchFormData": SearchFormData,
                                  "CartData": CartData,
                                  "CartBookingId": CartBookingId,
                                  "Passengers": [
                                    {
                                      "Type": "A",
                                      "Title": title,
                                      "FirstName": fname,
                                      "LastName": lname,
                                      "Preferences": [
                                        {
                                          "SeatPreference": "NA",
                                          "Id": "0|0",
                                          "MealPreference": "BBML",
                                          "SSRPreference": "AVIH"
                                        }
                                      ],
                                      "ReportingParameters": [
                                        {
                                          "Id": 7,
                                          "Value": "N"
                                        }
                                      ]
                                    }
                                  ],
                                  "DeliveryInfo": {
                                    "Mobile": mob,
                                    "Email": email,
                                    "SendEmail": "Y",
                                    "SendSMS": "Y"
                                  },
                                  "MetaInfos": {
                                    "E": "1",
                                    "M": "0"
                                  },
                                  "PaymentMethod": {
                                    "Mode": "D"
                                  },
                                  "GSTInfo": {
                                    "GSTNumber": "",
                                    "GSTName": "",
                                    "GSTPhone": "",
                                    "GSTAddress": "",
                                    "GSTEmail": ""
                                  }
                                }
                              });
                              
                              const xhr = new XMLHttpRequest();
                              xhr.withCredentials = true;
                              
                              xhr.addEventListener("readystatechange", function () {
                                if (this.readyState === 4) {
                                //   console.log(JSON.parse(this.responseText));
                                }
                              });
                              
                              xhr.open("POST", "https://test.sastiticket.com/restwebsvc/air/bookV1");
                              xhr.setRequestHeader("content-type", "application/json");
                              
                              xhr.send(data);
                        }
                      });
                      
                      xhr.open("POST", "https://test.sastiticket.com/restwebsvc/air/repriceV1");
                      xhr.setRequestHeader("content-type", "application/json");
                      
                      xhr.send(data);


                });



      //*****************************************************  flight_bookingdetails.html       End*/









    // ///////////////////////////// Core Function  


    function searchFlts(ap_from, ap_to, dep_date, ret_date, adult, children, infant, cabin_class) {
        const data = JSON.stringify({
            "Data": {
              "IsMobileSearchQuery": "N",
              "MaxOptionsCount": "30",
              "TravelType": "D",
              "JourneyType": "O",
              "IsPersonalBooking": "N",
              "AirOriginDestinations": [
                {
                  "DepartureAirport": ap_from,
                  "ArrivalAirport": ap_to,
                  "DepartureDate": dep_date
                }
              ],
              "AirPassengerQuantities": {
                "NumAdults": adult,
                "NumChildren": children,
                "NumInfants": infant
              },
              "AirSearchPreferences": {
                "BookingClass": cabin_class,
                "Carrier": "ANY"
              },
              "SearchLevelReportingParams": {
                "BillingEntityCode": 0
              }
            }
          });
          
          const xhr = new XMLHttpRequest();
          xhr.withCredentials = true;
          
          xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {

                if(ret_date != '') {
                    // console.log('if')
                    sessionStorage.setItem('f_res', this.responseText)
                    searchFlts(ap_to, ap_from, ret_date, '', adult, children, infant, cabin_class)
                }
                else {
                //   console.log('else')
                  if(sessionStorage.getItem('f_res')) 
                    sessionStorage.setItem('f_res1', this.responseText)
                  else
                    sessionStorage.setItem('f_res', this.responseText)
                    
                  if( sessionStorage.getItem('f_res') && sessionStorage.getItem('f_res1') ) 
                    window.location.href = 'search_ret.html'
                  else
                    window.location.href = 'search.html'
                }
            }
          });
          
          xhr.open("POST", "https://test.sastiticket.com/restwebsvc/air/searchV1");
          xhr.setRequestHeader("content-type", "application/json");
          xhr.withCredentials = true
          xhr.send(data);
    }


    function loadAirport() {
        $('.airport-select2').select2({
            ajax: {
                type: 'get',
                url: 'http://www.kashiyana.com/api/airport',
                dataType: 'json',
                data: function (params) {
                    var q = {
                        id: params.term,
                    }
                    return q;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (obj) {
                            return {
                                id: obj.iata_code,
                                text: obj.name
                            };
                        })
                    };
                }
            }
        });
    }


    function initDatePicker() {
        $('[data-toggle="datepicker"]').datepicker({
            format: 'dd/mm/yyyy',
            autoHide: true
        });
    }


    //  ///////////////////////////// Core Function End

});


